public class App {
    public static void main(String[] args) throws Exception {
       Time time1 = new Time(1, 20, 35);
       Time time2 = new Time(2, 25, 40);

       System.out.println("Time1: " + time1);
       System.out.println("Time2: " + time2);

       time1.nextSecond();
       time2.previousSecond();
       System.out.println("Time1 tang len 1 giay : " + time1);
       System.out.println("Time2 giam xuong 1 giay : " + time2);

    }
}
