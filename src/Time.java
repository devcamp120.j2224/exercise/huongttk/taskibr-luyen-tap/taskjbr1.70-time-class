public class Time {
    private int hour;
    private int minute;
    private int second;
    
    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public int getMinute() {
        return minute;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public int getSecond() {
        return second;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    @Override
    public String toString() {
        return "Time [hour = " + hour + ", minute = " + minute + ", second = " + second + "]";
    } 


    /////////////////////////
    public void nextSecond(){
        int sumSecond = this.second +1 ;
        if(sumSecond >=60){
            this.setSecond(0);
            //this.nextMinus();
        }
        else{
            this.setSecond(sumSecond);
            //this.setMinute(sumSecond);
        }
     }
     public void previousSecond(){
        int index = this.getSecond();
        if(index ==0){
            this.setSecond(59);
            //this.previousMinute();
        }
        else{
            this.setSecond(index-1);;
        }
     }
     /*
      *  public void nextHour(){
        this.setHour(this.hour + 1);
     }
     public void nextMinus(){
        int sumMinus = this.minute +1 ;
        if(sumMinus >=60){
            this.setMinute(0);
            this.nextHour();
        }
        else{
            this.setMinute(sumMinus);
        }
     }

      public void previousHour(){
        int index = this.getHour();
        if(index ==0){
            this.setHour(23);
        }
        else{
            this.setHour(this.hour - 1);
        }
       
     }
     public void previousMinute(){
        int index = this.getMinute();
        if(index ==0){
            this.setMinute(59);
            this.previousHour();
        }
        else{
            this.setMinute(index-1);;
        }
       
     }
      */
}
